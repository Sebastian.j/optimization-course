window.addEventListener('DOMContentLoaded', () => {
    $('[data-menu-open]').on('click', () => {
        $('[data-header]').addClass('open')
    })
    $('[data-menu-close]').on('click', () => {
        $('[data-header]').removeClass('open')
    })

});